package com.epamcourses.orestonatsko.parser.dom;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.dom.DOMSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;

class DOMValidator {

    static Schema createSchema(File xsd) {
        Schema schema = null;
        try {
            String language = XMLConstants.W3C_XML_SCHEMA_NS_URI;
            schema = SchemaFactory.newInstance(language).newSchema(xsd);
        } catch (SAXException e) {
            e.printStackTrace();
        }

        return schema;
    }

    static void validate(Schema schema, Document xml) {
        Validator validator = schema.newValidator();
        try {
            validator.validate(new DOMSource(xml));
        } catch (SAXException | IOException e) {
            e.printStackTrace();
        }
    }

}