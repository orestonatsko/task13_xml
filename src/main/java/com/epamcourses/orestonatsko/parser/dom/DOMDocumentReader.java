package com.epamcourses.orestonatsko.parser.dom;

import com.epamcourses.orestonatsko.model.Gem;
import com.epamcourses.orestonatsko.model.GemParameters;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

import static com.epamcourses.orestonatsko.model.Gem.*;

public class DOMDocumentReader {

    public List<Gem> readDoc(Document document) {
        document.getDocumentElement().normalize();
        List<Gem> list = new ArrayList<>();
        Node root = document.getElementsByTagName("gems").item(0);
        NodeList nodeList = root.getChildNodes();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            if (node.getNodeName().equalsIgnoreCase("gem"))
                list.add(getGem(node));
        }
        return list;
    }

    private Gem getGem(Node node) {
        Gem gem = new Gem();
        NodeList nodeList = node.getChildNodes();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node child = nodeList.item(i);
            getGemDetails(gem, child);
        }
        return gem;
    }

    private void getGemDetails(Gem gem, Node child) {
        switch (child.getNodeName()) {
            case NAME:
                gem.setName(child.getTextContent());
                break;
            case PRECIOUSNESS:
                gem.setPreciousness(Preciousness.valueOf(child.getTextContent().toUpperCase()));
                break;
            case ORIGIN:
                gem.setOrigin(child.getTextContent());
                break;
            case VALUE:
                gem.setValue(Integer.valueOf(child.getTextContent()));
                break;
            case PARAMETERS:
                gem.setParameters(getParameters(child));
                break;
        }
    }

    private GemParameters getParameters(Node child) {
        NodeList parameters = child.getChildNodes();
        String color = null;
        Integer transparency = null;
        Integer faces = null;
        for (int i = 0; i < parameters.getLength(); i++) {
            Node parameter = parameters.item(i);
            String name = parameter.getNodeName();
            if (name.equalsIgnoreCase("color")) {
                color = parameter.getTextContent();
            } else if (name.equalsIgnoreCase("transparency")) {
                transparency = Integer.valueOf(parameter.getTextContent());
            } else if (name.equalsIgnoreCase("faces")) {
                faces = Integer.valueOf(parameter.getTextContent());
            }
        }
        return new GemParameters(color, transparency, faces);
    }
}
