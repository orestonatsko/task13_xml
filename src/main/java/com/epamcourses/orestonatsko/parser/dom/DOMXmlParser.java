package com.epamcourses.orestonatsko.parser.dom;

import com.epamcourses.orestonatsko.parser.XmlParser;
import org.w3c.dom.Document;

import java.io.File;

public class DOMXmlParser extends XmlParser {

    @Override
    public void parse(File xml, File xsd) {
        DOMDocumentCreator creator = new DOMDocumentCreator();
        Document document = creator.getDocument(xml);

        DOMValidator.validate(DOMValidator.createSchema(xsd), document);
        DOMDocumentReader reader = new DOMDocumentReader();
        setElements(reader.readDoc(document));
    }
}
