package com.epamcourses.orestonatsko.parser.sax;

import com.epamcourses.orestonatsko.model.Gem;
import com.epamcourses.orestonatsko.model.GemParameters;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.List;

import static com.epamcourses.orestonatsko.model.Gem.*;
import static com.epamcourses.orestonatsko.model.GemParameters.*;

public class SAXHandler extends DefaultHandler {

    private List<Gem> gemList;
    private Gem gem;
    private GemParameters parameters;
    private StringBuilder data;

    private Boolean bName = false;
    private Boolean bOrigin = false;
    private Boolean bValue = false;
    private Boolean bPreciousness = false;
    private Boolean bParameters = false;
    private Boolean bColor = false;
    private Boolean bTransparency = false;
    private Boolean bFaces = false;

    public SAXHandler(List<Gem> gemList) {
        this.gemList = gemList;
    }


    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equalsIgnoreCase(GEM)) {
            gem = new Gem();
        } else if (qName.equalsIgnoreCase(NAME)) {
            bName = true;
        } else if (qName.equalsIgnoreCase(ORIGIN)) {
            bOrigin = true;
        } else if (qName.equalsIgnoreCase(VALUE)) {
            bValue = true;
        } else if (qName.equalsIgnoreCase(PRECIOUSNESS)) {
            bPreciousness = true;
        } else if (qName.equalsIgnoreCase(PARAMETERS)) {
            parameters = new GemParameters();
            bParameters = true;
        } else if (qName.equalsIgnoreCase(COLOR)) {
            bColor = true;
        } else if (qName.equalsIgnoreCase(TRANSPARENCY)) {
            bTransparency = true;
        } else if (qName.equalsIgnoreCase(FACES)) {
            bFaces = true;
        }
        data = new StringBuilder();
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equalsIgnoreCase(GEM)) {
            gemList.add(gem);
        } else if (bName) {
            gem.setName(data.toString());
            bName = false;
        } else if (bOrigin) {
            gem.setOrigin(data.toString());
            bOrigin = false;
        } else if (bValue) {
            gem.setValue(Integer.valueOf(data.toString().trim()));
            bValue = false;
        } else if (bPreciousness) {
            gem.setPreciousness(Gem.Preciousness.valueOf(data.toString().toUpperCase()));
            bPreciousness = false;
        } else if (bParameters) {
            gem.setParameters(parameters);
            bParameters = false;
        } else if (bColor) {
            parameters.setColor(data.toString());
            bColor = false;
        } else if (bTransparency) {
            parameters.setTransparency(Integer.valueOf(data.toString().trim()));
            bTransparency = false;
        } else if (bFaces) {
            parameters.setFaces(Integer.valueOf(data.toString().trim()));
            bFaces = false;
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        data.append(ch, start, length);
    }
}




