package com.epamcourses.orestonatsko.parser.sax;

import com.epamcourses.orestonatsko.model.Gem;
import com.epamcourses.orestonatsko.parser.XmlParser;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SAXXmlParser extends XmlParser {
    private SAXParserFactory factory;
    public SAXXmlParser(){
        factory = SAXParserFactory.newInstance();
    }
    @Override
    public void parse(File xml, File xsd) {
        factory.setSchema(SAXValidator.createSchema(xsd));
        List<Gem> gemList = new ArrayList<>();

        try {
            SAXParser parser = factory.newSAXParser();
            SAXHandler handler = new SAXHandler(gemList);
            parser.parse(xml, handler);
            setElements(gemList);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
    }
}
