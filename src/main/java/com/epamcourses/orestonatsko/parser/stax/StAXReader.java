package com.epamcourses.orestonatsko.parser.stax;

import com.epamcourses.orestonatsko.model.Gem;
import com.epamcourses.orestonatsko.model.GemParameters;
import com.epamcourses.orestonatsko.parser.XmlParser;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import static com.epamcourses.orestonatsko.model.Gem.*;
import static com.epamcourses.orestonatsko.model.GemParameters.*;

public class StAXReader extends XmlParser {

    private List<Gem> gemList = new ArrayList<>();
    private Gem gem;
    private GemParameters parameters;

    @Override
    public void parse(File xml, File xsd) {
        try {
            XMLEventReader reader = XMLInputFactory.newInstance().createXMLEventReader(new FileInputStream(xml));
            while (reader.hasNext()) {
                XMLEvent event = reader.nextEvent();
                if (event.isStartElement()) {
                    StartElement startElement = event.asStartElement();
                    switch (startElement.getName().getLocalPart()) {
                        case GEM:
                            gem = new Gem();
                            break;
                        case NAME:
                            event = reader.nextEvent();
                            gem.setName(event.asCharacters().getData());
                            break;
                        case ORIGIN:
                            event = reader.nextEvent();
                            gem.setOrigin(event.asCharacters().getData());
                            break;
                        case VALUE:
                            event = reader.nextEvent();
                            gem.setValue(Integer.valueOf(event.asCharacters().getData().trim()));
                            break;
                        case PRECIOUSNESS:
                            event = reader.nextEvent();
                            gem.setPreciousness(Preciousness.valueOf(event.asCharacters().getData().toUpperCase()));
                            break;
                        case PARAMETERS:
                            event = reader.nextEvent();
                            parameters = new GemParameters();
                            break;
                        case COLOR:
                            event = reader.nextEvent();
                            parameters.setColor(event.asCharacters().getData().trim());
                            break;
                        case TRANSPARENCY:
                            event = reader.nextEvent();
                            parameters.setTransparency(Integer.valueOf(event.asCharacters().getData().trim()));
                            break;
                        case FACES:
                            event = reader.nextEvent();
                            parameters.setFaces(Integer.valueOf(event.asCharacters().getData().trim()));
                            break;
                    }
                }
                if (event.isEndElement()){
                    EndElement endElement = event.asEndElement();
                    if (endElement.getName().getLocalPart().equalsIgnoreCase(GEM)){
                        gem.setParameters(parameters);
                        gemList.add(gem);
                    }
                }
            }
            setElements(gemList);
        } catch (FileNotFoundException | XMLStreamException e) {
            e.printStackTrace();
        }
    }
}
