package com.epamcourses.orestonatsko.parser;

import com.epamcourses.orestonatsko.model.Gem;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public abstract class XmlParser {
    private List<Gem> elements = new ArrayList<>();

    public abstract void parse(File xml, File xsd);

    public List<Gem> getElements() {
        return elements;
    }

    public void setElements(List<Gem> elements) {
        this.elements = elements;
    }
}
