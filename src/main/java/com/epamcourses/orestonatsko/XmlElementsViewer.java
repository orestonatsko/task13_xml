package com.epamcourses.orestonatsko;

import com.epamcourses.orestonatsko.menu.Command;
import com.epamcourses.orestonatsko.model.Gem;
import com.epamcourses.orestonatsko.parser.XmlParser;

import java.io.File;
import java.util.List;

public class XmlElementsViewer implements Command {
    private XmlParser parser;
    private File xml;
    private File xsd;

    public XmlElementsViewer(XmlParser parser, File xml, File xsd) {
        this.parser = parser;
        this.xml = xml;
        this.xsd = xsd;
    }

    @Override
    public void execute() {
        parser.parse(xml, xsd);
        List<Gem> elements = parser.getElements();
        view(elements);
    }

    private void view(List<Gem> elements) {
        for (Gem el : elements) {
            System.out.println(el);
        }
    }
}
