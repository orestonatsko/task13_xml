package com.epamcourses.orestonatsko.menu;

import com.epamcourses.orestonatsko.XmlElementsViewer;
import com.epamcourses.orestonatsko.parser.dom.DOMXmlParser;
import com.epamcourses.orestonatsko.parser.sax.SAXXmlParser;
import com.epamcourses.orestonatsko.parser.stax.StAXReader;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class Menu {
    private Map<String, String> menu;
    private Map<String, Command> methods;
    private Scanner input;

    private File xml;
    private File xsd;

    public Menu(File xml, File xsd) {
        this.xml = xml;
        this.xsd = xsd;
        input = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        methods = new HashMap<>();
        initMenu();

    }

    private void initMenu() {
        menu.put("1", "Get gems with DOM");
        menu.put("2", "Get gems with SAX");
        menu.put("3", "Get gems with StAX");
        menu.put("4", "Convert xml document to html");


        methods.put("1", new XmlElementsViewer(new DOMXmlParser(), xml, xsd));
        methods.put("2", new XmlElementsViewer(new SAXXmlParser(), xml, xsd));
        methods.put("3", new XmlElementsViewer(new StAXReader(), xml, xsd));

    }

    public void show() {
        String userInput;
        do {
            System.out.println("\n\t\t~~MENU~~");
            menu.forEach((k, v) -> System.out.println(k + " - " + v));
            System.out.println("Q - Quit");
            userInput = input.next();
            try {
                methods.get(userInput).execute();
            } catch (NullPointerException e) {/*ignore*/}
        } while (!userInput.equalsIgnoreCase("Q"));
    }
}