package com.epamcourses.orestonatsko;

import com.epamcourses.orestonatsko.menu.Menu;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;

public class App {
    public static void main(String[] args) {
        URL xmlUrl = App.class.getClassLoader().getResource("xml/gems.xml");
        URL xsdUrl = App.class.getClassLoader().getResource("xml/gemsXSD.xsd");
        File xml = null;
        File xsd = null;
        try {
            if (xmlUrl != null && xsdUrl != null) {
                xml = new File(xmlUrl.toURI());
                xsd = new File(xsdUrl.toURI());
            }
            new Menu(xml, xsd).show();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }
}
